$(document).ready(function () {
    getData();

});
function getData() {
    d3.csv("csv/data.csv", function (error, data) {
        if (error)
            throw error;
        else {
            var options = {};
            options.height = 500;
            options.lesslikely = 45;
            options.mostlikely = 20;
            plotAreaRangeChart("#areaRangeChart", data, options);
        }
    });
}

function plotAreaRangeChart(id, data, options) {
    var chartHeight = options.height ? options.height : 500;
    var lesslikely = options.lesslikely ? options.lesslikely : 45;
    var mostlikely = options.mostlikely ? options.mostlikely : 20;
    var randomIdString = Math.floor(Math.random() * 10000000000);
    var legendsArray = ["Average Market Performance", "Most Likely", "Less Likely"]
// set the dimensions and margins of the graph
    var margin = {top: 20, right: 50, bottom: 40, left: 20},
    width = $(id).width() - margin.left - margin.right,
            height = chartHeight - margin.top - margin.bottom;

// parse the date / time
    var parseTime = d3.time.format("%d-%b-%y").parse;

    bisectDate = d3.bisector(function (d) {
        return d.date;
    }).left; // **

// set the ranges
    var x = d3.time.scale().range([0, width]);
    var y = d3.scale.linear().range([height, 0]);
// Define the axes
    var xAxis = d3.svg.axis().scale(x)
            .orient("bottom").ticks(8).tickSize(0, 0);

    var yAxis = d3.svg.axis().scale(y)
            .orient("right").ticks(8).tickSize(0, 0).tickFormat(function (d) {
        return d + "%";
    });



// define the area
    var area = d3.svg.area()
            .x(function (d) {
                return x(d.date);
            })
            .y0(function (d) {
                return y(d.y0)
            })
            .y1(function (d) {
                return y(d.y1);
            });

    var line = d3.svg.line()
            .x(function (d) {
                return x(d.date);
            })
            .y(function (d) {
                return y(d.y)
            });


// append the svg obgect to the body of the page
// appends a 'group' element to 'svg'
// moves the 'group' element to the top left margin
    var tooltip_div = $(id).append("<div id='tooltipdiv' style='color:#C0C0C0; position: absolute;top: " + margin.top + "px;left: " + margin.left + "px;'><h2 style='color: black;opacity:0.6'>Expected Portfolio Returns</h2><div style='font-size:20px; padding:10px;margin: 5px; border: 1px solid #DFDFDF; width: 300px; height: 115px;'><div><h5 style='margin:0;padding:7px'>By <span id='tooltipdate'></span></h5></div><div id='tooltipdatadiv' style='font-size:16px'></div></div></div>")
    var svg = d3.select(id).append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform",
                    "translate(" + margin.left + "," + margin.top + ")");

    // format the data
    var data1 = [];// for most likely data
    var data2 = [];//for less likely data
    data.forEach(function (d) {

        d.date = parseTime(d.date);
        d.y = +d.y;
        data1.push({
            date: d.date,
            y1: d.y + ((d.y * mostlikely) / 100),
            y0: d.y - ((d.y * mostlikely) / 100)
        });
        data2.push({
            date: d.date,
            y1: d.y + ((d.y * lesslikely) / 100),
            y0: d.y - ((d.y * lesslikely) / 100)
        });
    });



    // Scale the range of the data
    x.domain(d3.extent(data, function (d) {

        return d.date;
    }));
    var ydomain = [d3.min(data2, function (d) {
            return d.y0;
        }), d3.max(data2, function (d) {
            return d.y1;
        })];

    y.domain([(ydomain[0] - (ydomain[0] * 20 / 100)), (ydomain[1] + (ydomain[1] * 20 / 100))]).nice();
//    console.log(y.domain(),data2)
// Add the X Axis
    var x_g = svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis).style({"fill": "none", "stroke": "#EEEEEE", "stroke-width": "1px", "shape-rendering": "crispEdges"});

    // Add the Y Axis
    var y_g = svg.append("g")
            .attr("class", "y axis")
            .attr('transform', 'translate(' + (width) + ',0)')
            .call(yAxis).style({"fill": "none", "stroke": "#EEEEEE", "stroke-width": "1px", "shape-rendering": "crispEdges"});


    x_g.selectAll("text").style({"fill": "#C0C0C0", "stroke": "none"});
    x_g.selectAll("text").each(function () {
        var y = parseFloat($(this).attr("y"));
        $(this).attr("y",y+7)
    })
    y_g.selectAll("text").style({"fill": "#C0C0C0", "stroke": "none"});
    y_g.selectAll("text").each(function () {
        var x = parseFloat($(this).attr("x"));
        $(this).attr("x", x+7)
    })


    // Add area for most likely
    svg.append("path")
            .data([data1])
            .attr("class", function (d) {
                return  "area legendCls_" + randomIdString + "_" + (("Most Likely").split(" ").join("___"));
            })
            .attr("d", area)
            .attr("data-legend", "Most Likely")
            .style("fill", "#073595")
            .style("opacity", "0.5");
    // Add area for less likely
    svg.append("path")
            .data([data2])
            .attr("class", function (d) {
                return  "area legendCls_" + randomIdString + "_" + (("Less Likely").split(" ").join("___"));
            })
            .attr("d", area)
            .attr("data-legend", "Less Likely")
            .style("fill", "#073595")
            .style("opacity", "0.2");
    // Add path for average
    svg.append("path")
            .data([data])
            .attr("class", function (d) {
                return  "area legendCls_" + randomIdString + "_" + (("Average Market Performance").split(" ").join("___"));
            })
            .attr("d", line)
            .attr("data-legend", "Average Market Performance")
            .style("stroke", "#073595")
            .style("fill", "#none")
            .style("stroke-width", "2px");

    $(id).append("<div style='color: #B2B2B2;' id='legendContainer-" + randomIdString + "'></div>");//append legend cotainer div
    renderLegend(legendsArray);//rendering legends 

//appends line and circles to svg for mouse over effect
    var mouseG = svg.append("g")
            .attr("class", "mouse-over-effects");
    mouseG.append("line").attr("y1", 0).attr("y2", height) // this is the black vertical line to follow mouse
            .attr("class", "mouse-line")
            .style("stroke", "#C1C4C9")
            .style("stroke-width", "2px")
            .style("opacity", "0");
    var focus = svg.append("g")
            .style("display", "none");
    // append the circle at the intersection               
    focus.append("circle")
            .attr("class", "AverageMP")
            .style("fill", "#6A85C0")
            .style("stroke", "#003293")
            .style("stroke-width", "3px")
            .attr("r", 8);
    focus.append("circle")
            .attr("class", "lessLikely")
            .style("fill", "#CDD6EA")
            .style("stroke", "#82A0DA")
            .style("stroke-width", "3px")
            .attr("r", 8);


// mouse over efect starts here
    svg.append("rect").attr("width", width).attr("height", height).style("fill", "transparent")
            .on('mouseout', function () { // on mouse out hide line, circles and text
                d3.select(".mouse-line")
                        .style("opacity", "0");

                focus.style("display", "none");
            })
            .on('mouseover', function () { // on mouse in show line, circles and text
                d3.select(".mouse-line")
                        .style("opacity", "1");
                focus.style("display", "block");
            })
            .on('mousemove', function () {   // mouse moving over canvas
                focus.style("display", "block");
                var mouse = d3.mouse(this);
                var x0 = x.invert(d3.mouse(this)[0])
                d3.select(".mouse-line").attr("x1", mouse[0]).attr("x2", mouse[0])

                //get intersection point of average path and mouseline
                var pathEl = svg.select(".legendCls_" + randomIdString + "_" + (("Average Market Performance").split(" ").join("___"))).node();
                var pathLength = pathEl.getTotalLength();
                var line = svg.select(".mouse-line");


                var points_for_avg = path_line_intersections(pathEl, line, pathLength);

                focus.select("circle.AverageMP")
                        .attr("transform",
                                "translate(" + d3.mouse(this)[0] + "," +
                                points_for_avg[0].y + ")");
                //get bad rate
                var pathEl = svg.select(".legendCls_" + randomIdString + "_" + (("Less Likely").split(" ").join("___"))).node();
                var pathLength = pathEl.getTotalLength();
                var points_for_bad = path_line_intersections(pathEl, line, pathLength);
                console.log(points_for_bad);
                focus.select("circle.lessLikely")
                        .attr("transform",
                                "translate(" + d3.mouse(this)[0] + "," +
                                points_for_bad[1].y + ")");
                //show tooltip data
                $("#tooltipdate").text(x0.toString().split(" ")[1] + " " + new Date(x0).getFullYear());
                $("#tooltipdatadiv").html("<div><div><div style='vertical-align: middle;width:13px;height:13px;border:3px solid #043695;border-radius:10px;background-color:#82A0DA;display:inline-block'></div><span><strong style='color:black;opacity:0.7;padding:7px'>" + parseFloat(y.invert(points_for_avg[0].y)).toFixed(2) + "% </strong> Expected Return</span></div><div style='margin-top:15px'><div style='vertical-align: middle;width:13px;height:13px;border:3px solid #82A0DA;border-radius:10px;background-color:#D4DCEC;display:inline-block'></div><span><strong style='color:black;opacity:0.7;padding:7px'>" + parseFloat(y.invert(points_for_bad[1].y)).toFixed(2) + "% </strong> If the Maket is Bad</span></div></div>")


            });
    var n_segments = 100;
    //--------------------------------------------------------------------------
    /**
     * Function to find path line intersection point
     */
    function path_line_intersections(pathEl, line, pathLength) {

        var pts = []
        for (var i = 0; i < n_segments; i++) {
            var pos1 = pathEl.getPointAtLength(pathLength * i / n_segments);
            var pos2 = pathEl.getPointAtLength(pathLength * (i + 1) / n_segments);
            var line1 = {x1: pos1.x, x2: pos2.x, y1: pos1.y, y2: pos2.y};
            var line2 = {x1: line.attr('x1'), x2: line.attr('x2'),
                y1: line.attr('y1'), y2: line.attr('y2')};
            var pt = line_line_intersect(line1, line2);
            if (typeof (pt) != "string") {
                pts.push(pt);
            }
        }

        return pts;

    }
    //--------------------------------------------------------------------------
    /**
     * Function to find line line intersection point
     */
    function line_line_intersect(line1, line2) {
        var x1 = line1.x1, x2 = line1.x2, x3 = line2.x1, x4 = line2.x2;
        var y1 = line1.y1, y2 = line1.y2, y3 = line2.y1, y4 = line2.y2;
        var pt_denom = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
        var pt_x_num = (x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4);
        var pt_y_num = (x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4);
        if (pt_denom == 0) {
            return "parallel";
        }
        else {
            var pt = {'x': pt_x_num / pt_denom, 'y': pt_y_num / pt_denom};
            if (btwn(pt.x, x1, x2) && btwn(pt.y, y1, y2) && btwn(pt.x, x3, x4) && btwn(pt.y, y3, y4)) {
                return pt;
            }
            else {
                return "not in range";
            }
        }
    }
    //--------------------------------------------------------------------------
    /**
     * 
     * Function to find whether in between or not
     */
    function btwn(a, b1, b2) {
        if ((a >= b1) && (a <= b2)) {
            return true;
        }
        if ((a >= b2) && (a <= b1)) {
            return true;
        }
        return false;
    }
    //--------------------------------------------------------------------------
    /**
     * Function to render legends
     */
    function renderLegend(labelObject) {
//create legends div
        var legenddiv = d3.select('#legendContainer-' + randomIdString);

//holder for each legend
        var legendHolder = legenddiv.selectAll('.legend-holder')
                .data(labelObject)
                .enter()
                .append('div')
                .attr('class', function (d) {
                    return 'legend-holder legend_holder_div_' + randomIdString + '_' + d;
                })
                .style('display', 'inline-block')
                .style('padding', '10px')
                .style('opacity', '1')
                .style('cursor', 'pointer');

//append legend circles
        legendHolder
                .append('div')
                .style('background-color', function (d, i) {
                    return "#003293"
                })
                .attr("class", "bar_legend_circles")
                .style("height", "8px")
                .style("width", "30px")
                .style("border-radius", "6px")
                .style("display", "inline-block")
                .style("opacity", function (d) {
                    return $(".legendCls_" + randomIdString + "_" + (d.split(" ").join("___"))).css("opacity");
                });

//append legend text
        legendHolder.append("p")
                .text(function (d, i) {
                    return d;
                })
                .style('display', 'inline-block')
                .style('margin-left', '5px');

    }
}